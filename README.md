# 16 - Monitoring with Prometheus - Monitoring for a Third-Party Application

**Demo Project:**
Configure Monitoring for a Third-Party Application

**Technologies used:**
Prometheus, Kubernetes, Redis, Helm, Grafana

**Project Description:**
Monitor Redis by using Prometheus Exporter
- Deploy Redis service in our cluster
- Deploy Redis exporter using Helm Chart
- Configure Alert Rules (when Redis is down or has too many connections)
- Import Grafana Dashboard for Redis to visualize monitoring data in Grafana




16 - Monitoring with Prometheus
# This project was developed as part of the DevOps Bootcamp at Techword With NANA https://www.techworld-with-nana.com/devops-bootcamp





